package com.itau.ranking;

import java.util.LinkedList;
import java.util.List;

import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.jms.Message;
import javax.jms.MessageListener;

import com.itau.ranking.models.Ranking;
import com.itau.ranking.repositories.RankingRepository;

public class Receiver implements MessageListener {

	RankingRepository rankingRepository;

	public Receiver(RankingRepository rankingRepository) {
		this.rankingRepository = rankingRepository;
	}

	public void onMessage(Message message) {
		MapMessage msg = (MapMessage) message;
		try {

			Ranking rank = new Ranking();

			rank.setPlayerName(msg.getString("playerName").toString());
			rank.setGameId((int) msg.getLong("gameId"));
			rank.setHits((int) msg.getInt("hits"));
			rank.setMisses((int) msg.getInt("misses"));
			rank.setTotal((int) msg.getInt("total"));

			rankingRepository.save(rank);

			System.out.println("------------ Ranking inserido com sucesso: " + rank + " ------------");

			message.acknowledge();

			// KAFKA
			String mensagem = "Grupo D - Ranking  do " + rank.getPlayerName() + " adicionado com sucesso! Pontuação: " + rank.getHits();
			long time = System.currentTimeMillis();

			List<Thread> threads = new LinkedList<>();

			System.out.println("Starting threads...");

			// Criar thread de produtores
			ProducerKafka producerKafka = new ProducerKafka();

			Thread t = producerKafka.createProducerThread(0, mensagem);

			// Inicia todas as threads
			t.start();

			try {
				System.out.println("Waiting threads...");
				t.join();

				time = System.currentTimeMillis() - time;

				System.out.println("Finished: " + time + "ms");

			} catch (InterruptedException e) {
				e.printStackTrace();
			}

		} catch (JMSException ex) {
			ex.printStackTrace();
		}
	}
}
