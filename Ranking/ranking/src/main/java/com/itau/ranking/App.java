package com.itau.ranking;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.jms.annotation.EnableJms;

import com.itau.ranking.repositories.RankingRepository;

@EnableJms
@SpringBootApplication
public class App {
		
	@Autowired
	static RankingRepository rakingRepository;
	
	public static void main(String[] args) {
		
		SpringApplication.run(App.class, args);
	}

}

