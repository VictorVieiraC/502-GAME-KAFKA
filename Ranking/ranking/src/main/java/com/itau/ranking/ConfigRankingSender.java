package com.itau.ranking;

import javax.jms.Connection;
import javax.jms.JMSException;
import javax.jms.MessageConsumer;
import javax.jms.Queue;
import javax.jms.Session;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import com.itau.ranking.repositories.RankingRepository;

@Component
public class ConfigRankingSender {

	@Autowired
	RankingRepository rankingRepository;
	
	@Bean
	private boolean consomeFilaSender() {

		try {
	    	ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory("tcp://amq.oramosmarcos.com:61616");
	    	Connection connection = connectionFactory.createConnection();
	    	connection.start();
	    	
	    	// Criar uma sessão
	    	Session session = connection.createSession(false, Session.CLIENT_ACKNOWLEDGE);
	    	
	    	// A fila que queremos popular
	    	Queue queue = session.createQueue("d.queue.ranking.id");
	    	
	    	// Criar um consumidor dessa fila
	    	MessageConsumer consumer = session.createConsumer(queue);
	    	consumer.setMessageListener(new Sender(session, rankingRepository));      
	    	
	    	System.out.println("Waiting messages...");
	    	
			} catch(JMSException ex) {
				ex.printStackTrace();
			}
		
			return true;
		}
	
}
