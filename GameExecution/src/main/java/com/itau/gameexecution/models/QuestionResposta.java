package com.itau.gameexecution.models;

import javax.persistence.Embeddable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.IdClass;

@Entity

public class QuestionResposta {

	
	@EmbeddedId
	private QuestionRespostaId id;

	
	private int resposta;
	

	public QuestionRespostaId getId() {
		return id;
	}

	public void setId(QuestionRespostaId id) {
		this.id = id;
	}

	public int getResposta() {
		return resposta;
	}

	public void setResposta(int resposta) {
		this.resposta = resposta;
	}



	
}
