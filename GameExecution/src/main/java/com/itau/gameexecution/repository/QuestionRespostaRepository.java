package com.itau.gameexecution.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import com.itau.gameexecution.models.QuestionResposta;
import com.itau.gameexecution.models.QuestionRespostaId;

public interface QuestionRespostaRepository extends CrudRepository<QuestionResposta, QuestionRespostaId>{
	public Optional<QuestionResposta> findByid(int id);
}
