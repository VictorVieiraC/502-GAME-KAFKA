package com.itau.questionsmicroservices;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.MessageProducer;
import javax.jms.Session;

import org.springframework.beans.factory.annotation.Autowired;

import com.itau.questionsmicroservices.models.Question;
import com.itau.questionsmicroservices.repository.QuestionRepository;

public class RequestListenerFull implements MessageListener {
	private static final int P_THREADS = 8;

	Session session;
	QuestionRepository questionRepository;

	public RequestListenerFull(Session session,  QuestionRepository questionRepository) {
		this.session = session;
		this.questionRepository = questionRepository;
	}

	public void onMessage(Message request) {
		MapMessage requestMap = (MapMessage) request;
		int id;
		try {
			System.out.println("NOVO PEDIDO DE PERGUNTA");
			id = requestMap.getInt("id");
			System.out.println("ID PEDIDO: " + id);

			MapMessage response = session.createMapMessage();
			response.setJMSCorrelationID(request.getJMSCorrelationID());
			
			Optional<Question> question = questionRepository.findByid(id);
			
			if (question.isPresent()) {
				response.setInt("status", 200);
				response.setString("title", question.get().getTitle());
				response.setString("category", question.get().getCategory());
				response.setString("option1", question.get().getOption1());
				response.setString("option2", question.get().getOption2());
				response.setString("option3", question.get().getOption3());
				response.setString("option4", question.get().getOption4());
				response.setInt("answer", question.get().getAnswer());
			}			

			MessageProducer producer = session.createProducer(request.getJMSReplyTo());
			producer.send(response);
			
			String mensagem = "Grupo D - Questão " + id + " devolvida para o jogo";
			request.acknowledge();
			
			long time = System.currentTimeMillis();
				
			List<Thread> threads = new LinkedList<>();	
			
			System.out.println("Starting threads...");
			
			// Criar thread de produtores
			//for (int i = 0; i < P_THREADS; i++) {
				ProducerKafka producerKfk = new ProducerKafka();
				Thread t = producerKfk.createProducerThread(String.valueOf(0), mensagem);
			//	threads.add(t);
			//}
			
			// Inicia todas as threads
			//for(Thread t : threads) {
				t.start();
			//}

			try {
				System.out.println("Waiting threads...");

				//for(Thread t : threads) {
					t.join();
				//}
				
				time = System.currentTimeMillis() - time;

				System.out.println("Finished: " + time + "ms");

			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			producer.close();
		} catch (JMSException e) {
			e.printStackTrace();
		}

	}
}
