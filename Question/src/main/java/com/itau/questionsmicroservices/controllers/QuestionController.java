package com.itau.questionsmicroservices.controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.itau.questionsmicroservices.models.Question;
import com.itau.questionsmicroservices.repository.QuestionRepository;

@RestController
@RequestMapping("/question")
@CrossOrigin
public class QuestionController {
	
	static RestTemplate restTemplate = new RestTemplate();
	
	@Autowired
	QuestionRepository questionRepository;

	@RequestMapping(method=RequestMethod.GET)
	public ResponseEntity<?> listQuestions(){
		return ResponseEntity.ok().body(questionRepository.findAll());
	}
	
	@RequestMapping(path="/{id}", method=RequestMethod.GET)
	public ResponseEntity<?> getQuestions(@PathVariable int id){
		return ResponseEntity.ok().body(questionRepository.findByid(id));
	}
	
	@RequestMapping(method=RequestMethod.POST)
	public ResponseEntity<?> setQuestion(@RequestBody Question question){
		return ResponseEntity.ok().body(questionRepository.save(question));
	}
	
	@RequestMapping(path="/{id}", method=RequestMethod.PUT)
	public ResponseEntity<?> updateQuestion(@RequestBody Question question, @PathVariable int id){
		Optional<Question> questionBanco = questionRepository.findByid(id);
		
		if (questionBanco.isPresent()) {
			question.setId(questionBanco.get().getId());
			return ResponseEntity.ok().body(questionRepository.save(question));			
		}
		return ResponseEntity.notFound().build();
	}
	
	@RequestMapping(path="/{id}", method=RequestMethod.DELETE)
	public ResponseEntity<?> deleteQuestion(@PathVariable int id){
		Optional<Question> questionBanco = questionRepository.findByid(id);
		
		if (questionBanco.isPresent()) {
			questionRepository.deleteById(id);
			return ResponseEntity.ok().build();			
		}
		return ResponseEntity.notFound().build();
	}
	
	@RequestMapping(path="/random/{id}", method=RequestMethod.GET)
	public ResponseEntity<?> NRandomQuestion(@PathVariable int id){
		Iterable<Question> listQuestion = questionRepository.findAll();
		List<Question> questions = new ArrayList<Question>();
		List<Question> questionsExit = new ArrayList<Question>();		
		
		for (Question question : listQuestion) {
			questions.add(question);
		}
		
		for (int i=0;i<id;i++) {
			int posicao = (int)Math.ceil(Math.random()*(questions.size()));	
			questionsExit.add(questions.get(posicao-1));
			questions.remove(posicao-1);
		}
		
		return ResponseEntity.ok().body(questionsExit);
	}
	
	@RequestMapping(path="/carregarbasefull", method=RequestMethod.GET)
	public ResponseEntity<?> CarregarBaseFullPistolaQuestion(){
		
		try {

			String url = "http://10.162.106.158:8000/all";
			//String response = restTemplate.getForObject(url, String.class);
					
			ResponseEntity<List<Question>> response = restTemplate.exchange(url, HttpMethod.GET, null , new ParameterizedTypeReference<List<Question>>(){});
			
			ArrayList<Question> questions = (ArrayList<Question>) response.getBody();
			
			for(Question question : questions) {
				questionRepository.save(question);	
			}
		
		} catch (Exception e) {
			return null;
		}
		
		return ResponseEntity.ok().build();
	}
	
	
}
