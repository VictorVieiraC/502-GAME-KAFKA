package com.itau.game.mq;

import java.util.List;
import java.util.Optional;

import javax.jms.Connection;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.Session;

import org.springframework.beans.factory.annotation.Autowired;

import com.itau.game.models.Game;
import com.itau.game.repositories.GameRepository;



public class Receiver{
	// QUAL EH A FILA ONDE SERAO BUSCADAS AS QUESTOES?
		private static String QUEUE_NAME = "d.queue.game.id";
		private static String QUEUE_NAME_FULL = "d.queue.game.full";
		
		private MessageProducer produtorSolicitacaoClient;
		
		private Destination filaTemporaria;
		
		private Queue filaComServer ;
		
		private Session sessaoCriada ;
		
		private 	MessageConsumer consumidorFilaTemporaria ;
		
		public final String randomQuestoesYes = "yes";
		public final String randomQuestoesNo = "no";
		
		private Connection connection;
		
		@Autowired
		GameRepository gameRepository;
		
	
		
		
		public Receiver(Connection criaConexaoBroker) {
			try {
				
				
					this.connection = criaConexaoBroker;
				// CRIAR UMA SESSAO
					sessaoCriada = connection.createSession(false, Session.CLIENT_ACKNOWLEDGE);
				
				// REFERENCIAR A FILA QUE QUEREMOS POPULAR
					 filaComServer = sessaoCriada.createQueue(QUEUE_NAME);
						
						
						
				} catch (Exception e) {
					System.out.println(e.getMessage());
				}
		}


		
		public void rReceiveGame(){
		
			try {
	        	
	        	MessageConsumer consumer = sessaoCriada.createConsumer(filaComServer);
	        
	        	consumer.setMessageListener(new RequestListener(sessaoCriada, gameRepository));

				//this.connection.close();
				

			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
			
			
		}



		

	
}
