package com.itau.game;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.ResponseEntity;

import com.itau.game.mq.ConexaoMq;
import com.itau.game.mq.Receiver;
import com.itau.game.repositories.GameRepository;



@SpringBootApplication
public class App 
{


    public static void main( String[] args )
    {
        SpringApplication.run(App.class, args);
        
        new Thread(new Runnable() {
        	@Override
        	public void run() {
        		
        		System.out.println("Lendo game:");
        		
        		Receiver receiver = new Receiver(ConexaoMq.CriaConexaoBroker());
        		
        		receiver.rReceiveGame();
        		
        		
        	}
        }).start();
        
    }
}
