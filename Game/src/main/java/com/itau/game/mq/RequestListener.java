package com.itau.game.mq;

import java.util.Optional;

import javax.jms.JMSException;
import javax.jms.MapMessage;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.MessageProducer;
import javax.jms.Session;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;

import com.itau.game.repositories.*;
import com.itau.game.models.*;

public class RequestListener implements MessageListener {

	Session session;
	
	GameRepository gameRepository;
	
	public RequestListener(Session session, GameRepository gameRepository) {
		this.session = session;
		this.gameRepository = gameRepository;
	}

	public void onMessage(Message request) {
		MapMessage requestMap = (MapMessage) request;
		int id;
		try {
			System.out.println("Chegou uma requisicao de " + request.getJMSCorrelationID());
			
			id = requestMap.getInt("idGame");
			
	
			
			Optional<Game> game = this.gameRepository.findById(id);
			
			MapMessage response = session.createMapMessage();
			response.setJMSCorrelationID(request.getJMSCorrelationID());
			
			response.setObject("game", game);

			MessageProducer producer = session.createProducer(request.getJMSReplyTo());
			producer.send(response);
			
			producer.close();
		} catch (Exception e) {
			
			System.out.println(e.getMessage());
			
			e.printStackTrace();
		}

	}
}
